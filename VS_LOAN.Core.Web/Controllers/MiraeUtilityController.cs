﻿using MCreditService;
using MCreditService.Interfaces;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using VS_LOAN.Core.Business.Interfaces;
using VS_LOAN.Core.Entity.MCreditModels;
using VS_LOAN.Core.Entity.MCreditModels.SqlModel;
using VS_LOAN.Core.Repository.Interfaces;
using PdfSharp.Pdf;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using PdfSharp;
using VS_LOAN.Core.Web.ModelExport;
using System.Net.Http;
using System.Net.Http.Headers;

namespace VS_LOAN.Core.Web.Controllers
{
    public partial class MiraeUtilityController : BaseController
    {

        protected readonly IMiraeRepository _rpMCredit;
        protected readonly IMediaBusiness _bizMedia;
        protected readonly IMiraeService _miraeService;
        public readonly IOcbBusiness _ocbBusiness;
        public readonly IMiraeMaratialRepository _rpTailieu;
        public static ProvinceResponseModel _provinceResponseModel;
        public MiraeUtilityController(IMiraeRepository rpMCredit,
              IMediaBusiness mediaBusiness,
            IMiraeService odcService, IOcbBusiness ocbBusiness, IMiraeMaratialRepository tailieuBusiness) : base()
        {
            _rpTailieu = tailieuBusiness;
            _rpMCredit = rpMCredit;
            _miraeService = odcService;
            _bizMedia = mediaBusiness;
            _ocbBusiness = ocbBusiness;


        }

        public ActionResult GetFilePdf()
        {
            
            return View();

        }

        static string[] mNumText = "không;một;hai;ba;bốn;năm;sáu;bảy;tám;chín".Split(';');
        //Viết hàm chuyển số hàng chục, giá trị truyền vào là số cần chuyển và một biến đọc phần lẻ hay không ví dụ 101 => một trăm lẻ một
        private static string DocHangChuc(double so, bool daydu)
        {
            string chuoi = "";
            //Hàm để lấy số hàng chục ví dụ 21/10 = 2
            Int64 chuc = Convert.ToInt64(Math.Floor((double)(so / 10)));
            //Lấy số hàng đơn vị bằng phép chia 21 % 10 = 1
            Int64 donvi = (Int64)so % 10;
            //Nếu số hàng chục tồn tại tức >=20
            if (chuc > 1)
            {
                chuoi = " " + mNumText[chuc] + " mươi";
                if (donvi == 1)
                {
                    chuoi += " mốt";
                }
            }
            else if (chuc == 1)
            {//Số hàng chục từ 10-19
                chuoi = " mười";
                if (donvi == 1)
                {
                    chuoi += " một";
                }
            }
            else if (daydu && donvi > 0)
            {//Nếu hàng đơn vị khác 0 và có các số hàng trăm ví dụ 101 => thì biến daydu = true => và sẽ đọc một trăm lẻ một
                chuoi = " lẻ";
            }
            if (donvi == 5 && chuc >= 1)
            {//Nếu đơn vị là số 5 và có hàng chục thì chuỗi sẽ là " lăm" chứ không phải là " năm"
                chuoi += " lăm";
            }
            else if (donvi > 1 || (donvi == 1 && chuc == 0))
            {
                chuoi += " " + mNumText[donvi];
            }
            return chuoi;
        }
        private static string DocHangTram(double so, bool daydu)
        {
            string chuoi = "";
            //Lấy số hàng trăm ví du 434 / 100 = 4 (hàm Floor sẽ làm tròn số nguyên bé nhất)
            Int64 tram = Convert.ToInt64(Math.Floor((double)so / 100));
            //Lấy phần còn lại của hàng trăm 434 % 100 = 34 (dư 34)
            so = so % 100;
            if (daydu || tram > 0)
            {
                chuoi = " " + mNumText[tram] + " trăm";
                chuoi += DocHangChuc(so, true);
            }
            else
            {
                chuoi = DocHangChuc(so, false);
            }
            return chuoi;
        }
        private static string DocHangTrieu(double so, bool daydu)
        {
            string chuoi = "";
            //Lấy số hàng triệu
            Int64 trieu = Convert.ToInt64(Math.Floor((double)so / 1000000));
            //Lấy phần dư sau số hàng triệu ví dụ 2,123,000 => so = 123,000
            so = so % 1000000;
            if (trieu > 0)
            {
                chuoi = DocHangTram(trieu, daydu) + " triệu";
                daydu = true;
            }
            //Lấy số hàng nghìn
            Int64 nghin = Convert.ToInt64(Math.Floor((double)so / 1000));
            //Lấy phần dư sau số hàng nghin 
            so = so % 1000;
            if (nghin > 0)
            {
                chuoi += DocHangTram(nghin, daydu) + " nghìn";
                daydu = true;
            }
            if (so > 0)
            {
                chuoi += DocHangTram(so, daydu);
            }
            return chuoi;
        }
        public static string ChuyenSoSangChuoi(double so)
        {
            if (so == 0)
                return mNumText[0];
            string chuoi = "", hauto = "";
            Int64 ty;
            do
            {
                //Lấy số hàng tỷ
                ty = Convert.ToInt64(Math.Floor((double)so / 1000000000));
                //Lấy phần dư sau số hàng tỷ
                so = so % 1000000000;
                if (ty > 0)
                {
                    chuoi = DocHangTrieu(so, true) + hauto + chuoi;
                }
                else
                {
                    chuoi = DocHangTrieu(so, false) + hauto + chuoi;
                }
                hauto = " tỷ";
            } while (ty > 0);
            return chuoi + " đồng";
        }
        private string GetAddressDetail  (int idtinh, int idHuyen, int idthanhpho, string addressInfo)
        {

            var ddlTinh = MiraeService.AllProvince.Where(x => x.Stateid == idtinh.ToString()).FirstOrDefault();
            var tinhText = "";
            if(ddlTinh!=null)
            {
                tinhText = ddlTinh.Statedesc;
            }

            var ddlHuyen = MiraeService.AllDistrict.Where(x => x.Lmc_CITYID_C == idHuyen.ToString()).FirstOrDefault();
            var huyenText = "";
            if (ddlHuyen != null)
            {
                huyenText = ddlHuyen.Lmc_CITYNAME_C;
            }
      
            var ddlPHuong = MiraeService.AllWard.Where(x => x.Zipcode == idthanhpho.ToString()).FirstOrDefault();
            var phuongText = "";
            if (ddlPHuong != null)
            {
                
                phuongText = ddlPHuong.Zipdesc;
            }
            else
            {

                ddlPHuong = MiraeService.AllWard.Where(x => x.Zipcode ==  "0"+ idthanhpho).FirstOrDefault();
                if(ddlPHuong!=null)
                {
                    phuongText = ddlPHuong.Zipdesc;
                }

            }

            return string.Concat(addressInfo, " ", phuongText, " ", huyenText, " ", tinhText);
        }


        private static string getNameRelationShip(string relationshipCode)
        {
            switch (relationshipCode)
            {
                case "R":
                    return "Người thân";
                case "CA":
                    return "Đồng vay";
                case "WH":
                    return "Vợ chồng";
                case "F":
                    return "Bạn bè";
                case "C":
                    return "Đồng nghiệp";
                default:
                    return string.Empty;

            }
          }
        [HttpGet]
        public  HttpResponseMessage printPdf(int id = 324)
        {


            var fileContents = System.IO.File.ReadAllText("Content/file.html");


            var response = new HttpResponseMessage();
            response.Content = new StringContent(fileContents);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            return response;
            //var miraeItem = await _rpMCredit.GetDetail(id);

            var miraeItem = new MiraeDetailModel {
        IsQDEToDDE = true,
        IsDDeCreate = true,
        IsQDEToPor = true,
        IsPushDoucment = true,
        S37IdReqested = null,
        Mobile =  "0352413881",
        Fixphone = null,
        AppId = "2866387",
        Id = 2714,
        Channel = null,
        Schemeid = "739",
        Downpayment = 0,
        Totalloanamountreq = 20000000,
        Tenure = 24,
        Sourcechannel = "ADVT",
        Salesofficer = "42821",
        Loanpurpose = "A",
        Creditofficercode = null,
        Bankbranchcode = null,
        Laa_app_ins_applicable = "True",
        Possipbranch = null,
        Priority_c = "Bank Statement",
        Userid = null,
        Fname = "TRẦN",
        Mname = "HUỲNH",
        Lname = "NGỌC",
        Nationalid = "072302006453",
        Title = "MS",
        Gender = "1",
        Dob =  new DateTime(2022,08,30),
        Constid = 0,
        Tax_code = "3901157636",
        Presentjobyear = 2,
        Presentjobmth = 2,
        Previousjobyear = 0,
        Previousjobmth = 0,
        Natureofbuss = "hoat dong lam thue cac cong viec trong cac hgd, sx sp vat chat va dich vu tu tieu dung cua ho gia dinh",
        Referalgroup = "3",
        Addresstype = "HEADOFF",
        Addressline = "Lô số 34 - 6,  Đường D11,KCN Phước Đông",
        Phone = "02763530999",
        Others = "CÔNG TY TNHH BROTEX(VIỆT NAM)",
        Position = "CÔNG NHÂN",
        Head = null,
        Frequency = null,
        Amount = "6200000",
        Accountbank = "0",
        Debit_credit = null,
        In_per_cont = null,
        AddressCur_in_propertystatus = "F",
        AddressCur_address1stline = "Tổ 21, Ri 197 Tua Hai",
        AddressCur_Country = 46,
        AddressCur_City = 46,
        AddressCur_District = 524,
        AddressCur_Ward = 25594,
        AddressCur_roomno = null,
        AddressCur_stayduratcuradd_y = "17",
        AddressCur_stayduratcuradd_m = "0",
        AddressCur_mailingaddress = null,
        AddressCur_mobile = null,
        AddressCur_landlord = null,
        AddressCur_landmark = null,
        Refferee1_in_title = "MR",
        Refferee1_Refereename = "Trần Quốc Hải - Bố đẻ",
        Refferee1_Refereerelation = "R",
        Refferee1_Phone1 = "0335324733",
        Refferee1_Phone2 = "0335324733",
        Refferee2_in_title = "MR",
        Refferee2_Refereename = "Nguyễn Thanh Nam - ĐỒNG NGHIỆP",
        Refferee2_Refereerelation = "C",
        Refferee2_Phone1 = "0967570470",
        Refferee2_Phone2 = "0967570470",
        Refferee3_in_title = null,
        Refferee3_Refereename = null,
        Refferee3_Refereerelation = null,
        Refferee3_Phone1 = null,
        Refferee3_Phone2 = null,
        AddressPer_in_propertystatus = "O",
        AddressPer_address1stline = null,
        AddressPer_Country = 189,
        AddressPer_City = 0,
        AddressPer_District = 0,
        AddressPer_Ward = 0,
        AddressPer_roomno = null,
        AddressPer_stayduratPeradd_y = null,
        AddressPer_stayduratPeradd_m = null,
        AddressPer_mailingaddress = null,
        AddressPer_mobile = null,
        AddressPer_landlord = null,
        AddressPer_landmark = null,
        ContryCompany = 189,
        CityCompany = 46,
        DistrictCompany = 527,
        WardCompany = 25672,
        IsDuplicateAdrees = true,
        Maritalstatus = "S",
        Qualifyingyear = "0",
        Eduqualify = "HG",
        Noofdependentin = "0",
        Paymentchannel = "O",
        Familybooknumber = "DN4727222454990",
        Idissuer = "CỤC TRƯỞNG CỤC CS QLHC VỀ TTXH",
        Spousename = null,
        Spouse_id_c = null,
        Categoryid = "SBK",
        Bankname = "01201001",
        Bankbranch = "CN TAY NINH",
        Acctype = "CURRENT",
        Accno = "101873120682",
        Dueday = "10",
        DuedayRecomend = "10",
        Notecode = "DE_MOBILE",
        Notedetails = null,
        PrivateInfo = null,
        PrivateInfoOther = null,
        NotedDetailPrivate = null,
        Spouse_phoneNumber = null,
        Spouse_companyName = null,
        Spouse_addressName = null,
        Status = 20,
        NotedDetailPrivate2 = null,
        CreatedTime = DateTime.Now,
        CreatedBy = 5786,
        UpdatedTime = DateTime.Now,
        UpdatedBy = 0

    };

    var eduqualify = miraeItem.Eduqualify;

    var eduqualifyText = miraeItem.Eduqualify;

            var currentAddressInfo = GetAddressDetail(miraeItem.AddressCur_City, miraeItem.AddressCur_District, miraeItem.AddressCur_Ward, miraeItem.AddressCur_address1stline);
            var permetAddressInfo = GetAddressDetail(miraeItem.AddressPer_City, miraeItem.AddressPer_District, miraeItem.AddressPer_Ward, miraeItem.AddressPer_address1stline);
            var companyInfoInfo = GetAddressDetail(miraeItem.CityCompany, miraeItem.DistrictCompany, miraeItem.WardCompany, miraeItem.Addressline);

                    
            var schemeid = miraeItem.Schemeid;
            switch (miraeItem.Eduqualify)
            {

                case "CE":
                    eduqualifyText = " Cao đăng hoặc tương đương";
                    break;
                case "HG":
                    eduqualifyText = "Phổ thông(lớp12)";
                    break;
                case "LG":
                    eduqualifyText = "Dưới phổ thông";
                    break;
                case "U":
                    eduqualifyText = "Đại học";
                    break;

                case "UU":
                    eduqualifyText = "Cao học";
                    break;
                default:
                    break;
            }



            var totalloanamountreqText = "";
            var totalloanamountreqTextInclue = "";
            if (miraeItem.Totalloanamountreq >0)
            {
                totalloanamountreqText = ChuyenSoSangChuoi(Decimal.ToDouble(miraeItem.Totalloanamountreq.Value));



                var percent = 6.6;

                var totalinlcudebaohien = Decimal.ToDouble(miraeItem.Totalloanamountreq.Value) + Decimal.ToDouble(miraeItem.Totalloanamountreq.Value) * percent / 100;

                totalloanamountreqTextInclue = ChuyenSoSangChuoi(totalinlcudebaohien);
            }

            else
            {


            }
            var amountText = "";

            if (miraeItem.Amount != "")
            {
                amountText = ChuyenSoSangChuoi(Double.Parse (miraeItem.Amount));
            }

            else
            {


            }  
            var sanphamvayText = "";
            var sanphamvayObject = MiraeService.Allproduct.Where(x => x.Schemeid.ToString() == miraeItem.Schemeid).FirstOrDefault();

            if (sanphamvayObject !=null)
            {
                sanphamvayText = sanphamvayObject.Schemename;

            }

            var bankNameText = "";


            var bankNameObject = MiraeService.AllBank.Where(x => x.Bankid.ToString() == miraeItem.Bankname).FirstOrDefault();

            if(bankNameObject != null)
            {
                bankNameText = bankNameObject.Bankdesc;
            }
            var data = new MiraeExport()
            {

                FullName = string.Concat(miraeItem.Fname, " ", miraeItem.Mname, " ", miraeItem.Lname),
                Dob = miraeItem.Dob,
                Nationalid = miraeItem.Nationalid,
                Nationalidissuedate = miraeItem.Nationalidissuedate,
                Notedetails = miraeItem.Notedetails,
                Mobile = miraeItem.Mobile,
                Maritalstatus = miraeItem.Maritalstatus,
                Eduqualify = miraeItem.Eduqualify,
                Noofdependentin = miraeItem.Noofdependentin,
                EduqualifyText = eduqualifyText,
                CurrentAddressText = currentAddressInfo,
                PermetAddressText = permetAddressInfo,
                CompanyAddressText = companyInfoInfo,
                AddressCur_stayduratcuradd_m = miraeItem.AddressCur_stayduratcuradd_m,
                AddressCur_stayduratcuradd_y = miraeItem.AddressCur_stayduratcuradd_y,
                AddressCur_in_propertystatus = miraeItem.AddressCur_in_propertystatus,
                AddressCur_mobile = miraeItem.AddressCur_mobile,
                AddressCur_landlord = miraeItem.AddressCur_landlord,
                AddressCur_roomno = miraeItem.AddressCur_roomno,
                AddressCur_landmark = miraeItem.AddressCur_landmark,
                IsDuplicateAdrees = miraeItem.IsDuplicateAdrees,
                Familybooknumber = miraeItem.Familybooknumber,
                AddressPer_mobile = miraeItem.AddressPer_mobile,
                Loanpurpose = miraeItem.Loanpurpose,
                Dueday = miraeItem.Dueday,
                Totalloanamountreq = miraeItem.Totalloanamountreq,
                TotalloanamountreqText = totalloanamountreqText,
                Tenure = miraeItem.Tenure,
                Gender = miraeItem.Gender,
                Amount = miraeItem.Amount,
                AmountText = amountText,
                Priority_c = miraeItem.Priority_c,
                Others = miraeItem.Others,
                AddressCompanyInfo = companyInfoInfo,
                Tax_code = miraeItem.Tax_code,
                Presentjobyear = miraeItem.Presentjobyear,
                Presentjobmth = miraeItem.Presentjobmth,
                Position = miraeItem.Position,
                Refferee1_in_title = miraeItem.Refferee1_in_title,
                Refferee1_Phone1 = miraeItem.Refferee1_Phone1,
                Refferee1_Refereename = miraeItem.Refferee1_Refereename,
                Refferee1_Refereerelation = getNameRelationShip(miraeItem.Refferee1_Refereerelation),
                Refferee1_Phone2 = miraeItem.Refferee1_Phone2,


                Refferee2_in_title = miraeItem.Refferee2_in_title,
                Refferee2_Phone1 = miraeItem.Refferee2_Phone1,
                Refferee2_Refereename = miraeItem.Refferee2_Refereename,
                Refferee2_Refereerelation = getNameRelationShip(miraeItem.Refferee2_Refereerelation),
                Refferee2_Phone2 = miraeItem.Refferee2_Phone2,

                Refferee3_in_title = miraeItem.Refferee3_in_title,
                Refferee3_Phone1 = miraeItem.Refferee3_Phone1,
                Refferee3_Refereename = miraeItem.Refferee3_Refereename,
                Refferee3_Refereerelation = getNameRelationShip(miraeItem.Refferee3_Refereerelation),
                Refferee3_Phone2 = miraeItem.Refferee3_Phone2,
                Laa_app_ins_applicable = miraeItem.Laa_app_ins_applicable,
                Spousename = miraeItem.Spousename,
                Spouse_id_c = miraeItem.Spouse_id_c,

                Accountbank = miraeItem.Accountbank,
                SanphamvayText = sanphamvayText,
                Bankname = miraeItem.Bankname,
                Accno = miraeItem.Accno,
                Acctype = miraeItem.Acctype,

                Bankbranch = miraeItem.Bankbranch,
                Bankbranchcode = miraeItem.Bankbranchcode,
                BankNameText = bankNameText,
                Idissuer = miraeItem.Idissuer,
                NotedDetailPrivate2 = miraeItem.NotedDetailPrivate2,
                PrivateInfo = miraeItem.PrivateInfo,
                PrivateInfoOther = miraeItem.PrivateInfoOther,
                NotedDetailPrivate = miraeItem.NotedDetailPrivate,

                Spouse_phoneNumber = miraeItem.Spouse_phoneNumber,
                Spouse_companyName = miraeItem.Spouse_companyName,
                Spouse_addressName = miraeItem.Spouse_addressName,
                DuedayRecomend = miraeItem.DuedayRecomend,
                Phone = miraeItem.Phone,


                TotalloanamountreqTextInclue = totalloanamountreqTextInclue
            
               



        };


            //return View();
        }




    }
}