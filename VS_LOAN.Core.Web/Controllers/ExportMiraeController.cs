﻿using MCreditService.Interfaces;
using MCreditService.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using VS_LOAN.Core.Repository;
using VS_LOAN.Core.Repository.Interfaces;
using VS_LOAN.Core.Entity;
using VS_LOAN.Core.Entity.MCreditModels;
using VS_LOAN.Core.Entity.MCreditModels.SqlModel;
using VS_LOAN.Core.Entity.Model;
using VS_LOAN.Core.Entity.UploadModel;
using VS_LOAN.Core.Web.Helpers;
using VS_LOAN.Core.Business.Interfaces;
using VS_LOAN.Core.Utility;
using Newtonsoft.Json;
using System.IO.Compression;
using VS_LOAN.Core.Utility.OfficeOpenXML;
using VS_LOAN.Core.Utility.Exceptions;

namespace VS_LOAN.Core.Web.Controllers
{
    public class ExportMiraeController : BaseController
    {
 
        protected readonly IMiraeRepository _rpMCredit;
        protected readonly IMiraeService _svMCredit;
        protected readonly INoteRepository _rpNote;

        protected readonly IMediaBusiness _bizMedia;
        protected readonly ITailieuRepository _rpTailieu;
        protected readonly IEmployeeRepository _rpEmployee;
        protected readonly ILogRepository _rpLog;
        public ExportMiraeController(IMiraeRepository rpMCredit,
            INoteRepository noteRepository,
          
            IMediaBusiness mediaBusiness,
            ITailieuRepository tailieuRepository,
            ILogRepository logRepository,
            IEmployeeRepository employeeRepository,
            IMiraeService loanContractService) : base()
        {
            _rpMCredit = rpMCredit;
            _svMCredit = loanContractService;
            _rpNote = noteRepository;
            _bizMedia = mediaBusiness;
            
            _rpTailieu = tailieuRepository;
            _rpEmployee = employeeRepository;
            _rpLog = logRepository;
        }


        public async Task<ActionResult> DownloadReportAsync(string freeText, string status, int page = 1, int limit = 10, string fromDate = null, string toDate = null, int loaiNgay = 0, int manhom = 0,

          int mathanhvien = 0)
        {
            page = page <= 0 ? 1 : page;
            limit = 10000;



            string newUrl = string.Empty;
            try
            {
       
                DateTime dtFromDate = DateTime.Now.AddDays(-90), dtToDate = DateTime.Now;
                if (fromDate != "")
                    dtFromDate = DateTimeFormat.ConvertddMMyyyyToDateTimeNew(fromDate);
                if (toDate != "")
                    dtToDate = DateTimeFormat.ConvertddMMyyyyToDateTimeNew(toDate);

                var profiles = await _rpMCredit.GetTempProfiles(page, limit, freeText, GlobalData.User.IDUser, status, dtFromDate, dtToDate, loaiNgay, manhom, mathanhvien = 0);
                if (profiles == null || !profiles.Any())
                {
                    return ToJsonResponse(true, "", DataPaging.Create(null as List<ProfileSearchSql>, 0));
                }
                string destDirectory = VS_LOAN.Core.Utility.Path.DownloadBill + "/" + DateTime.Now.Year.ToString() + "/" + DateTime.Now.Month.ToString() + "/";
                bool exists = System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + destDirectory);
                if (!exists)
                    System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + destDirectory);
                string fileName = "Mirae" + DateTime.Now.ToString("ddMMyyyyHHmmssfff") + ".xlsx";
                using (FileStream stream = new FileStream(Server.MapPath(destDirectory + fileName), FileMode.CreateNew))
                {
                    Byte[] info = System.IO.File.ReadAllBytes(Server.MapPath(VS_LOAN.Core.Utility.Path.ReportTemplate + "ReportMirae.xlsx"));
                    stream.Write(info, 0, info.Length);
                    using (ZipArchive archive = new ZipArchive(stream, ZipArchiveMode.Update))
                    {
                        string nameSheet = "data";
                        ExcelOOXML excelOOXML = new ExcelOOXML(archive);
                        int rowindex = 4;
                        if (profiles.Any())
                        {
                            excelOOXML.InsertRow(nameSheet, rowindex, profiles.Count - 1, true);
                            for (int i = 0; i < profiles.Count; i++)// dòng
                            {
                                var item = profiles[i];
                                excelOOXML.SetCellData(nameSheet, "A" + rowindex, (i + 1).ToString());
                                excelOOXML.SetCellData(nameSheet, "B" + rowindex, item.Id.ToString());
                                excelOOXML.SetCellData(nameSheet, "C" + rowindex, item.AppId);
                                excelOOXML.SetCellData(nameSheet, "D" + rowindex, item.Fname + " " + item.Mname +" "+ item.Lname);
                                excelOOXML.SetCellData(nameSheet, "E" + rowindex, item.Nationalid);
                                excelOOXML.SetCellData(nameSheet, "G" + rowindex, item.StatusName);
                                excelOOXML.SetCellData(nameSheet, "G" + rowindex, item.Statusclient);
                                excelOOXML.SetCellData(nameSheet, "H" + rowindex, item.Notedetails);
                                excelOOXML.SetCellData(nameSheet, "I" + rowindex, item.LastNoteVietBank);
                                excelOOXML.SetCellData(nameSheet, "J" + rowindex, item.CreatedUser);
                                excelOOXML.SetCellData(nameSheet, "K","");

                                excelOOXML.SetCellData(nameSheet, "L" + rowindex, item.CreatedTime.ToString());
                                excelOOXML.SetCellData(nameSheet, "M" ,"");


                                //excelOOXML.SetCellData(nameSheet, "F" + rowindex, item.Notecode);
                                //decimal? totlcurrosnumber = null;
                                //try
                                //{
                                //    totlcurrosnumber = Convert.ToDecimal(item.TotalCurros);
                                //}
                                //catch (Exception)
                                //{
                                //totlcurrosnumber != null ? String.Format("{0:n0}", totlcurrosnumber) : ""
                                //    totlcurrosnumber = null;
                                //}

                                //excelOOXML.SetCellData(nameSheet, "G" + rowindex,item.ProductName );

                                //excelOOXML.SetCellData(nameSheet, "H" + rowindex, item.McNote);
                                //excelOOXML.SetCellData(nameSheet, "I" + rowindex, item.LastNote);
                                //excelOOXML.SetCellData(nameSheet, "J" + rowindex, item.StatusName);
                                //excelOOXML.SetCellData(nameSheet, "H" + rowindex, item.PaymentAppointmentDate != null ? item.PaymentAppointmentDate.ToString() : "");
                                //excelOOXML.SetCellData(nameSheet, "I" + rowindex, item.PaymentAppointmentAmount != null ? String.Format("{0:n0}", item.PaymentAppointmentAmount) : "");
                                //excelOOXML.SetCellData(nameSheet, "J" + rowindex, item.AssigneeName);
                                //excelOOXML.SetCellData(nameSheet, "K" + rowindex, item.LoanPeriodName);
                                //excelOOXML.SetCellData(nameSheet, "L" + rowindex, item.CreatedUser);
                                //excelOOXML.SetCellData(nameSheet, "M" + rowindex,"");
                                //excelOOXML.SetCellData(nameSheet, "N" + rowindex, item.CreatedTime.ToString());

                                //excelOOXML.SetCellData(nameSheet, "O" + rowindex, item.UpdatedTime.ToString());
                                //excelOOXML.SetCellData(nameSheet, "P" + rowindex, item.SaleName.ToString());
                                rowindex++;
                            }
                        }
                        archive.Dispose();
                    }
                    stream.Dispose();
                }

                newUrl = "/File/GetFile?path=" + destDirectory + fileName;
                return ToResponse(true, "", newUrl);

            }
            catch (BusinessException ex)
            {
                return ToResponse(false, ex.Message);
            }

        }

    }
}