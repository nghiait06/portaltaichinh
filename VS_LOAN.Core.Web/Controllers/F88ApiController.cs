﻿using MCreditService;
using MCreditService.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using VS_LOAN.Core.Business.Interfaces;
using VS_LOAN.Core.Entity;
using VS_LOAN.Core.Entity.MCreditModels;
using VS_LOAN.Core.Entity.MCreditModels.SqlModel;
using VS_LOAN.Core.Entity.Model;
using VS_LOAN.Core.Entity.UploadModel;
using VS_LOAN.Core.Repository;
using VS_LOAN.Core.Repository.Interfaces;
using VS_LOAN.Core.Utility;
using VS_LOAN.Core.Web.Helpers;
using System.Net.Http;
using System.Text;



using System.Net;

using System.Security.Principal;

using System.Threading;

using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http;
using Unity;
using System.Dynamic;

namespace VS_LOAN.Core.Web.Controllers
{
   

    public class F88ApiController : BaseApiController
    {




        public readonly IMiraeDeferRepository _miraeDeferRepository;
        public readonly IMiraeRepository _miraeRepository;
        public readonly IMiraeService _miraeService;
        public readonly IMiraeMaratialRepository _miraeMaratialRepository;
        public F88ApiController()
        {
            IUnityContainer container = new UnityContainer();

            container.RegisterType<IMiraeDeferRepository, MiraeDeferRepository>();
            container.RegisterType<INoteRepository, NoteRepository>();
            container.RegisterType<IMiraeRepository, MiraeRepository>();
            container.RegisterType<IMiraeMaratialRepository, MiraeMaratialRepository>();
            container.RegisterType<ILogRepository, LogRepository>();
            container.RegisterType<IMiraeService, MiraeService>();
            container.RegisterType<IMiraeMaratialRepository, MiraeMaratialRepository>();

            _miraeService = container.Resolve<IMiraeService>();
            _miraeDeferRepository = container.Resolve<IMiraeDeferRepository>();
            _miraeRepository = container.Resolve<IMiraeRepository>();
            _miraeMaratialRepository = container.Resolve<IMiraeMaratialRepository>();
        }

        public F88ApiController(IMiraeDeferRepository miraeDeferRepository) : base()
        {


            this._miraeDeferRepository = miraeDeferRepository;


        }

        [BasicAuthentication]

        public async Task<HttpResponseMessage> UploadStatus(F88Request requests)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, requests);
            var item = new F88Reponse();

            var profile = await _miraeRepository.UpdateStatusF88(requests.ReferenceType, requests.ReferenceId,
                requests.StatusF88, requests.F88Note, requests.LoanMoneyOrg, requests.LastComment);
            item.IsSucess = true;

            item.Message = "Cập nhật thành công";
            return this.Request.CreateResponse(HttpStatusCode.OK, item);
        }

      

        

    }
}