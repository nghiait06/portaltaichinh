﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VS_LOAN.Core.Entity.MCreditModels.SqlModel;

namespace VS_LOAN.Core.Web.ModelExport
{
    public class MiraeExport : MiraeDetailModel
    {

       public MiraeExport ()
        {
        }
        
        public string SanphamvayText { get; set; }


        public string TotalloanamountreqText { get; set; }

        public string AmountText { get; set; }
        public string EduqualifyText { get; set; }
        public string FullName { get; set; } 
        public string TotalloanamountreqTextInclue { get; set; }
        public string DobText { get {

        return Dob.ToString("dd/MM/yyyy");
        }
        
        }
        public string CurrentAddressText { get; set; }
        public string PermetAddressText { get; set; }
        public string CompanyAddressText { get; set; }
        public string BankNameText { get; set; }
         public string AddressCompanyInfo { get; set; }
        
    }
}