﻿using System;

namespace VS_LOAN.Core.Entity.MCreditModels
{
    public class MiraeSerarchSql 
    {


        public string In_channel { get; set; }
        public string In_schemeid { get; set; }

        public decimal? In_downpayment { get; set; }
        public decimal? In_totalloanamountreq { get; set; }

        public int In_tenure { get; set; }

        public string In_sourcechannel { get; set; }

        public string In_salesofficer { get; set; }

        public string In_loanpurpose { get; set; }

        public string In_creditofficercode { get; set; }

        public string In_bankbranchcode { get; set; }

        public string In_laa_app_ins_applicable { get; set; }
        public string In_possipbranch { get; set; }

        public string In_priority_c { get; set; }

        public string In_userid { get; set; }

        public string In_fname { get; set; }

        public string In_mname { get; set; }

        public string In_lname { get; set; }

        public string In_nationalid { get; set; }

        public string In_title { get; set; }

        public string In_gender { get; set; }

        public DateTime? In_dob { get; set; }

        public int In_constid { get; set; }

        public string In_tax_code { get; set; }

        public int In_presentjobyear { get; set; }

        public int In_presentjobmth { get; set; }

        public int In_previousjobyear { get; set; }

        public int In_previousjobmth { get; set; }

        public string In_natureofbuss { get; set; }

        public string In_referalgroup { get; set; }

        public string In_addresstype { get; set; }

        public string In_addressline { get; set; }

        public int In_country { get; set; }

        public int In_city { get; set; }

        public int In_district { get; set; }

        public int In_ward { get; set; }

        public string In_phone { get; set; }

        public string In_others { get; set; }

        public string In_position { get; set; }

        public string In_head { get; set; }


        public string In_frequency { get; set; }

        public string In_amount { get; set; }

        public string In_accountbank { get; set; }

        public string In_debit_credit { get; set; }

        public string In_per_cont { get; set; }

        public string MsgName { get; set; }

        public string Reference1In_title { get; set; }

        public string Reference1In_refereename { get; set; }
        public string Reference1In_refereerelation { get; set; }

        public string Reference1In_phone_1 { get; set; }

        public string Reference1In_phone_2 { get; set; }


        public string Reference2In_title { get; set; }

        public string Reference2In_refereename { get; set; }
        public string Reference2In_refereerelation { get; set; }

        public string Reference2In_phone_1 { get; set; }

        public string Reference2In_phone_2 { get; set; }

        public string Address1In_addresstype { get; set; }

        public string Address1In_propertystatus { get; set; }
        public string Address1In_address1stline { get; set; }

        public int Address1In_country { get; set; }
        public int Address1In_city { get; set; }
        public int Address1In_district { get; set; }
        public int Address1In_ward { get; set; }
        public string Address1In_roomno { get; set; }
        public int Address1In_stayduratcuradd_y { get; set; }
        public int Address1In_stayduratcuradd_m { get; set; }
        public string Address1In_mailingaddress { get; set; }
        public string Address1In_mobile { get; set; }
        public string Address1In_landlord { get; set; }
        public string Address1In_landmark { get; set; }

        public string Address2In_addresstype { get; set; }
        public string Address2In_propertystatus { get; set; }
        public string Address2In_Address2stline { get; set; }

        public int Address2In_country { get; set; }
        public int Address2In_city { get; set; }
        public int Address2In_district { get; set; }
        public int Address2In_ward { get; set; }
        public string Address2In_roomno { get; set; }
        public int Address2In_stayduratcuradd_y { get; set; }
        public int Address2In_stayduratcuradd_m { get; set; }
        public string Address2In_mailingaddress { get; set; }
        public string Address2In_mobile { get; set; }
        public string Address2In_landlord { get; set; }
        public string Address2In_landmark { get; set; }



        public string Vbf { get; set; }

        public string VbFUserUpdate { get; set; }

       




    }
}

