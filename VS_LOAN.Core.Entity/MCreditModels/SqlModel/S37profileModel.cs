﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VS_LOAN.Core.Entity.MCreditModels.SqlModel
{
    public class S37profileModel
    {
        public DateTime CreatedTime { get; set; }
        public int CreatedBy { get; set; }

        public int Id { get; set; }

        public string CMND { get; set; }

        public string Content { get; set; }

        public int Suscess { get; set; }

        public string TypeRecord { get; set; }

        public string RequestId { get; set; }

        public bool? IsDeleted { get; set; }

        public string DataMessage { get; set; }

        

    }

}