﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VS_LOAN.Core.Entity.MCreditModels
{


    public class Mirae3PRequest 
    {
      
        public string in_appid { get; set; }
  

        public string in_maritalstatus { get; set; }
        public string in_qualifyingyear { get; set; }

        public string in_eduqualify { get; set; }
        public string in_noofdependentin { get; set; }
        public string in_paymentchannel { get; set; }

        public string in_nationalidissuedate { get; set; }

        public string in_familybooknumber { get; set; }

        public string in_idissuer { get; set; }

        public string in_spousename { get; set; }

        public string in_spouse_id_c { get; set; }

        public string in_categoryid { get; set; }

        public string in_bankname { get; set; }
        public string in_bankbranch { get; set; }

        public string in_acctype { get; set; }



        public string in_accno { get; set; }

        public string in_dueday { get; set; }

        public string in_notecode { get; set; }

        public string in_notedetails { get; set; }

        public string in_channel { get; set; }
        public int? in_schemeid { get; set; }
        public decimal? in_downpayment { get; set; }
        public decimal? in_totalloanamountreq { get; set; }

        public string in_sourcechannel { get; set; }
        public int? in_tenure { get; set; }
        public string in_salesofficer { get; set; }
        public string in_loanpurpose { get; set; }
        public string in_creditofficercode { get; set; }
        public string in_bankbranchcode { get; set; }
        public string in_laa_app_ins_applicable { get; set; }
        public string in_possipbranch { get; set; }
        public string in_priority_c { get; set; }
        public string in_userid { get; set; }
        public string in_fname { get; set; }
        public string in_mname { get; set; }

        public string in_lname { get; set; }
        public string in_nationalid { get; set; }
        public string in_title { get; set; }
        public string in_gender { get; set; }
        public string in_dob { get; set; }
       
        public List<AddressResolveItem> address { get; set; }
        public string in_tax_code { get; set; }
        public int? in_presentjobyear { get; set; }

        public int? in_presentjobmth { get; set; }

        public int? in_previousjobyear { get; set; }

        public int? in_previousjobmth { get; set; }

        public string in_natureofbuss { get; set; }

        public string in_referalgroup { get; set; }

        public string in_addresstype { get; set; }

        public string in_addressline { get; set; }

        public int? in_country { get; set; }

        public int? in_city { get; set; }
        public int? in_district { get; set; }

        public int? in_ward { get; set; }

        public string in_phone { get; set; }

        public string in_others { get; set; }

        public string in_position { get; set; }



        public List<ReferenceItem> reference { get; set; }
        public string msgName { get; set; }

        public string in_head { get; set; }

        public string in_frequency { get; set; }

        public string in_amount { get; set; }

        public string in_accountbank { get; set; }

        public string in_debit_credit { get; set; }
        public string in_per_cont { get; set; }

        public string in_mobile { get; set; }
        public string in_fixphone { get; set; }
        


    }

    public class Mirae3PReponse
    {

        public bool Success { get; set; }
        public string Message { get; set; }

        public object Data { get; set; }

        public Mirae3PReponse()
        {
            Success = false;
        }

    }

   
}



