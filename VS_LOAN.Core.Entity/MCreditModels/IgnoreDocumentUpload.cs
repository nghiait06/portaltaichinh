﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VS_LOAN.Core.Entity.MCreditModels
{
    public class IgnoreDocumentUpload
    {
        public int GroupId { get; set; }
        public string DocumentCode { get; set; }
    }
}
