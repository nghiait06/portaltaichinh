﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VS_LOAN.Core.Entity.MCreditModels.SqlModel
{
    public class F88Request
    {
        public string ReferenceType { get; set; }

        public string ReferenceId { get; set; }
        public string StatusF88{ get; set; }

        public string F88Note { get; set; }

        public string LoanMoneyOrg { get; set; }

        public string LastComment { get; set; }


    }

    public class F88Reponse
    {
        public bool IsSucess { get; set; }
        public string Message { get; set; }

        public F88Reponse()
        {
            IsSucess = false;

        }
    }


}
