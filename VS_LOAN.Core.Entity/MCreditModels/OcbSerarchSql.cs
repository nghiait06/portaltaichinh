﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VS_LOAN.Core.Entity.MCreditModels
{
    public class OcbSerarchSql : BaseSqlEntity
    {
        public string idIssueDate;
        public int TotalRecord { get; set; }
        public int Id { get; set; }

        public string TraceCode { get; set; }

        public string FullNamme { get; set; }

        public bool? Gender { get; set; }

        public string IdCard { get; set; }
        public string idIssuePlaceId { get; set; }
        public DateTime BirthDay { get; set; }

        public string Mobilephone { get; set; }

        public decimal? InCome { get; set; }

        public string CurAddressWardId { get; set; }

        public string RurAddressDistId { get; set; }

        public string CurAddressProvinceId { get; set; }

        public string RegAddressWardId { get; set; }

        public string RegAddressDistId { get; set; }

        public string RegAddressProvinceId { get; set; }

        public string CurAddressDistId { get; set; }

        public string ProductId { get; set; }

        public string SellerNote { get; set; }


        public decimal? RequestLoanAmount { get; set; }

        public int? RequestLoanTerm { get; set; }

        public string RegAddressNumber { get; set; }

        public string RegAddressStreet { get; set; }
        public string RegAddressRegion { get; set; }
        public string CurAddressNumber { get; set; }

        public string CurAddressStreet { get; set; }

        public string CurAddressRegion { get; set; }

        public string IncomeType { get; set; }
        public string Email { get; set; }

        public int? AssigneeId { get; set; }

        public string CreatedBy { get; set; }

        public string CreatedUser { get; set; }

        public string UpdateUser { get; set; }

        public string FirstCallStatus { get; set; }

        public string LastCallStatus { get; set; }

        public DateTime? LastCallDate { get; set; }

        public string lastCallNote { get; set; }

        public string AppProcessStatus { get; set; }

        public string AppStatusForSale { get; set; }

        public string RejectCode { get; set; }

        public string LastNoteVietBank { get; set; }

        public string CustomerId { get; set; }

        public string StatusName { get; set; }

        public DateTime? DisbureseDate { get; set; }

        public string DisbureseMonth { get; set; }
        public decimal? Volumn { get; set; }

        public string CancelCode { get; set; }

        public string Vbf { get; set; }

        public string VbFUserUpdate { get; set; }

       




    }
}

